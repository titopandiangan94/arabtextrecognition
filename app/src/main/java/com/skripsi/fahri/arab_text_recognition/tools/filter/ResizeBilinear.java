package com.skripsi.fahri.arab_text_recognition.tools.filter;

import com.skripsi.fahri.arab_text_recognition.tools.InterfaceBitmap;

public class ResizeBilinear implements InterfaceBitmap {

    private int newWidth;
    private int newHeight;

    /**
     * Get Width of the new resized image.
     * @return New width.
     */
    public int getNewWidth() {
        return newWidth;
    }

    /**
     * Set Width of the new resized image.
     * @param newWidth New width.
     */
    public void setNewWidth(int newWidth) {
        this.newWidth = newWidth;
    }

    /**
     * Get Height of the new resized image.
     * @return New height.
     */
    public int getNewHeight() {
        return newHeight;
    }

    /**
     * Set Height of the new resized image.
     * @param newHeight New height.
     */
    public void setNewHeight(int newHeight) {
        this.newHeight = newHeight;
    }

    /**
     * Set Size of the new resized image.
     * @param newWidth Width of the new resized image.
     * @param newHeight Height of the new resized image.
     */
    public void setNewSize(int newWidth, int newHeight){
        this.newWidth = newWidth;
        this.newHeight = newHeight;
    }

    /**
     * Initialize a new instance of the ResizeNearestNeighbor class.
     * @param newWidth Width of the new resized image.
     * @param newHeight Height of the new resize image.
     */
    public ResizeBilinear(int newWidth, int newHeight) {
        this.newWidth = newWidth;
        this.newHeight = newHeight;
    }

    @Override
    public void applyInPlace(FastBitmap fastBitmap) {

        FastBitmap dest = new FastBitmap(newWidth, newHeight, fastBitmap.getColorSpace());

        int width = fastBitmap.getWidth();
        int height = fastBitmap.getHeight();
        double jFactor = (double)width / (double)newWidth;
        double iFactor = (double)height / (double)newHeight;

        if(fastBitmap.isGrayscale()){
            for (int i = 0; i < newHeight; i++) {

                int I = (int)(i * iFactor);
                int p;

                for (int j = 0; j < newWidth; j++) {

                    int J = (int)(j * jFactor);
                    p = fastBitmap.getGray(I, J);

                    dest.setGray(i, j, p);
                }
            }

            fastBitmap.setImage(dest);
            dest.recycle();
        }
        else{
            for (int i = 0; i < newHeight; i++) {

                int I = (int)(i * iFactor);
                int r, g, b;

                for (int j = 0; j < newWidth; j++) {

                    int J = (int)(j * jFactor);
                    r = fastBitmap.getRed(I, J);
                    g = fastBitmap.getGreen(I, J);
                    b = fastBitmap.getBlue(I, J);

                    dest.setRGB(i, j, r, g, b);
                }
            }

            fastBitmap.setImage(dest);
            dest.recycle();
        }
    }
}