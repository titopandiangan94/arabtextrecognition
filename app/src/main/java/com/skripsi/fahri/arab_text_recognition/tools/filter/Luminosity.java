package com.skripsi.fahri.arab_text_recognition.tools.filter;

import android.graphics.Bitmap;
import android.graphics.Color;

public class Luminosity {
    public static Bitmap proses(Bitmap img)     {

        Bitmap bmap = img.copy(Bitmap.Config.ARGB_8888, true);
        for (int i = 0; i < bmap.getWidth(); i++){
            for (int j = 0; j < bmap.getHeight(); j++){
                int pixel = img.getPixel(i, j);
                // mengambil warna dari semua channel
                int A = Color.alpha(pixel);
                int R = Color.red(pixel);
                int G = Color.green(pixel);
                int B = Color.blue(pixel);


                R = G = B = (int)(0.299 * R + 0.587 * G + 0.114 * B);
                // memuat image ke dengan nilai pixel yang baru
                bmap.setPixel(i, j, Color.argb(A, R, G, B));
            }
        }
        return bmap.copy(Bitmap.Config.ARGB_8888, true);
    }
}
