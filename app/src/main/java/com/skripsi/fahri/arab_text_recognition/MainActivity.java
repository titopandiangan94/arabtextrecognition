package com.skripsi.fahri.arab_text_recognition;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.googlecode.tesseract.android.TessBaseAPI;
import com.skripsi.fahri.arab_text_recognition.tools.filter.BilinearInterpolation;
import com.skripsi.fahri.arab_text_recognition.tools.filter.Luminosity;
import com.skripsi.fahri.arab_text_recognition.utils.Constant;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ListIterator;
import java.util.Objects;

import static com.skripsi.fahri.arab_text_recognition.utils.Constant.PERMISSION_REQUEST_CODE;
import static com.skripsi.fahri.arab_text_recognition.utils.Constant.RESULT_LOAD_IMAGE;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class MainActivity extends AppCompatActivity {

    String language=null;
    String TAG=null;
    Button btnPilihImage,btnPrepro,btnOcr,btnProsesLansung;
    Bitmap image, imageToOcr,newImage;
    ImageView imgView;
    TextView tvHasilOcr,tvWaktuProses,tvMessage;
    private TessBaseAPI mTess;
    String datapath = "";
    String filepath;
    int Seconds, Minutes, MilliSeconds ;
    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    Handler handler;
    LinearLayout lansung, tidakLansung;
    Toolbar toolbar;
    ProgressBar progressBar;
    TextView tvResolusi;
    LinearLayout layoutData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        validasiView(getIntent().getIntExtra("status",0));

        handler= new Handler();
        language= Constant.languange;
        TAG=Constant.TAG;

        initDataView();
        eventLister();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            image = BitmapFactory.decodeFile(picturePath, options);


            if (image!=null){
                newImage= Bitmap.createScaledBitmap(image,200,100,false);
                btnPrepro.setEnabled(true);
                btnPilihImage.setEnabled(false);
                btnProsesLansung.setEnabled(true);
                tvMessage.setVisibility(View.GONE);
                //imgView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                imgView.setImageBitmap(newImage);
            }


            /*try {
                Toast.makeText(MainActivity.this, "Disini", Toast.LENGTH_LONG).show();

                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                image = BitmapFactory.decodeStream(imageStream);
                imgView.setImageBitmap(image);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }*/

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }

    private void initView(){
        btnPilihImage=findViewById(R.id.pilihImage);
        imgView=findViewById(R.id.imgView);
        btnOcr=findViewById(R.id.btnOcr);
        btnPrepro=findViewById(R.id.btnPrepro);
        btnProsesLansung=findViewById(R.id.btnProsesLansung);

        lansung=findViewById(R.id.layoutLansung);
        tidakLansung=findViewById(R.id.layoutTidaklansung);

        tvHasilOcr=findViewById(R.id.tvHasilOcr);
        tvWaktuProses=findViewById(R.id.tvWaktuProses);
        tvResolusi=findViewById(R.id.tvResolusi);

        progressBar=findViewById(R.id.progresbar);

        tvMessage=findViewById(R.id.tvMessage);
        toolbar=findViewById(R.id.toolbar);

        layoutData= findViewById(R.id.layoutData);
    }
    private void validasiView(int i){
        if (i==Constant.lansung)
        {
            lansung.setVisibility(View.VISIBLE);
        }
        else{
            tidakLansung.setVisibility(View.VISIBLE);
        }
    }
    private void initDataView(){
        //imgView.setImageDrawable(getResources().getDrawable(R.drawable.test_ara));
        datapath = getFilesDir()+ "/tesseract/";
        filepath = datapath + "/tessdata/";
        mTess = new TessBaseAPI();


        //Pengecekan File data Training apakah ada di HP.
        checkFileTrained(new File(datapath + "tessdata/"));
        checkFileBigrams(new File(datapath + "tessdata/"));
        checkFileFold(new File(datapath + "tessdata/"));
        checkFileLm(new File(datapath + "tessdata/"));
        checkFileNn(new File(datapath + "tessdata/"));
        checkFileParams(new File(datapath + "tessdata/"));
        checkFileSize(new File(datapath + "tessdata/"));
        checkFileWordFreq(new File(datapath + "tessdata/"));

        //Inisiasi tesseract
        mTess.init(datapath, language);
        mTess.setDebug(true);
        mTess.setPageSegMode(TessBaseAPI.OEM_CUBE_ONLY);
    }
    private void eventLister(){
        //Action untuk memilih image ketika di tekan pilih image
        btnPilihImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()){
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                }
                else {
                    requestPermission();
                }

            }
        });

        //Action untuk pre-processing jika di tekan tombol pre-processing
        btnPrepro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bmp= newImage.copy(Bitmap.Config.ARGB_8888,true);
                Bitmap bmpBilinear= BilinearInterpolation.Resize(bmp);
                Bitmap bmpLuminosity= Luminosity.proses(bmpBilinear);
                imageToOcr=bmpLuminosity;
                imgView.setImageBitmap(bmpLuminosity);
                btnOcr.setEnabled(true);
                btnPrepro.setEnabled(false);
            }
        });

        //Acation untuk text recogniton dengan tesseract
        btnOcr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //fungsi untuk me-recognition image
                showProgress(progressBar,layoutData);
                processImage(imageToOcr);

                btnOcr.setEnabled(false);
                btnPilihImage.setEnabled(true);
                stopProgres(progressBar,layoutData);
            }
        });

        btnProsesLansung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startTimer();
                showProgress(progressBar,layoutData);
                processImage(newImage);

                btnProsesLansung.setEnabled(false);
                btnPilihImage.setEnabled(true);
                stopProgres(progressBar,layoutData);
            }
        });

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(MainActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    public void processImage(Bitmap imgToOcr){

        long startTime = System.currentTimeMillis();
        String OCRresult = null;
        mTess.setImage(imgToOcr);
        tvResolusi.setText(imgToOcr.getWidth()+" x "+imgToOcr.getHeight()+" pixel");
        OCRresult = mTess.getUTF8Text();  // proses merecognise image
        Toast.makeText(getBaseContext(),OCRresult, Toast.LENGTH_SHORT).show();
        tvHasilOcr.setText(OCRresult);   /// untuk menampilkan hasil recognition
        long stopTime = System.currentTimeMillis();
        tvWaktuProses.setText(String.valueOf(stopTime-startTime)+" ms");


        /*try {
            //startTimer();
            String OCRresult = null;
            mTess.setImage(imgToOcr);
            OCRresult = mTess.getUTF8Text();
            Toast.makeText(getBaseContext(),OCRresult, Toast.LENGTH_SHORT).show();
            tvHasilOcr.setText(OCRresult);
            pauseTimer();
        }catch (Exception e){
            e.printStackTrace();
            pauseTimer();
        }*/

    }

    private void checkFileTrained(File dir) {
        if (!dir.exists()&& dir.mkdirs()){
            copyFilesTrained();
        }
        if(dir.exists()) {
            String datafilepath = datapath+ "/tessdata/ara.traineddata";
            File datafile = new File(datafilepath);

            if (!datafile.exists()) {
                copyFilesTrained();
            }
        }
    }
    private void checkFileFold(File dir) {
        if (!dir.exists()&& dir.mkdirs()){
            copyFilesFold();
        }
        if(dir.exists()) {
            String datafilepath = datapath+ "/tessdata/ara.cube.fold";
            File datafile = new File(datafilepath);

            if (!datafile.exists()) {
                copyFilesFold();
            }
        }
    }
    private void checkFileLm(File dir) {
        if (!dir.exists()&& dir.mkdirs()){
            copyFilesLm();
        }
        if(dir.exists()) {
            String datafilepath = datapath+ "/tessdata/ara.cube.lm";
            File datafile = new File(datafilepath);

            if (!datafile.exists()) {
                copyFilesLm();
            }
        }
    }
    private void checkFileNn(File dir) {
        if (!dir.exists()&& dir.mkdirs()){
            copyFilesNn();
        }
        if(dir.exists()) {
            String datafilepath = datapath+ "/tessdata/ara.cube.nn";
            File datafile = new File(datafilepath);

            if (!datafile.exists()) {
                copyFilesNn();
            }
        }
    }
    private void checkFileParams(File dir) {
        if (!dir.exists()&& dir.mkdirs()){
            copyFilesParams();
        }
        if(dir.exists()) {
            String datafilepath = datapath+ "/tessdata/ara.cube.params";
            File datafile = new File(datafilepath);

            if (!datafile.exists()) {
                copyFilesParams();
            }
        }
    }
    private void checkFileSize(File dir) {
        if (!dir.exists()&& dir.mkdirs()){
            copyFilesSize();
        }
        if(dir.exists()) {
            String datafilepath = datapath+ "/tessdata/ara.cube.size";
            File datafile = new File(datafilepath);

            if (!datafile.exists()) {
                copyFilesSize();
            }
        }
    }
    private void checkFileWordFreq(File dir) {
        if (!dir.exists()&& dir.mkdirs()){
            copyFilesWordFreq();
        }
        if(dir.exists()) {
            String datafilepath = datapath+ "/tessdata/ara.cube.word-freq";
            File datafile = new File(datafilepath);

            if (!datafile.exists()) {
                copyFilesWordFreq();
            }
        }
    }
    private void checkFileBigrams(File dir) {
        if (!dir.exists()&& dir.mkdirs()){
            copyFilesBigrams();
        }
        if(dir.exists()) {
            String datafilepath = datapath+ "/tessdata/ara.cube.bigrams";
            File datafile = new File(datafilepath);

            if (!datafile.exists()) {
                copyFilesBigrams();
            }
        }
    }


    //Mengcopy file training ke internal memory (semua fungsi yg ada tulisan copy)
    private void copyFilesBigrams() {
        try {

            String filepath = datapath + "/tessdata/ara.cube.bigrams";
            AssetManager assetManager = getAssets();

            InputStream instream = assetManager.open("tessdata/ara.cube.bigrams");
            OutputStream outstream = new FileOutputStream(filepath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }


            outstream.flush();
            outstream.close();
            instream.close();

            File file = new File(filepath);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void copyFilesFold() {
        try {

            String filepath = datapath + "/tessdata/ara.cube.fold";
            AssetManager assetManager = getAssets();

            InputStream instream = assetManager.open("tessdata/ara.cube.fold");
            OutputStream outstream = new FileOutputStream(filepath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }


            outstream.flush();
            outstream.close();
            instream.close();

            File file = new File(filepath);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void copyFilesLm() {
        try {

            String filepath = datapath + "/tessdata/ara.cube.lm";
            AssetManager assetManager = getAssets();

            InputStream instream = assetManager.open("tessdata/ara.cube.lm");
            OutputStream outstream = new FileOutputStream(filepath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }


            outstream.flush();
            outstream.close();
            instream.close();

            File file = new File(filepath);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void copyFilesNn() {
        try {

            String filepath = datapath + "/tessdata/ara.cube.nn";
            AssetManager assetManager = getAssets();

            InputStream instream = assetManager.open("tessdata/ara.cube.nn");
            OutputStream outstream = new FileOutputStream(filepath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }


            outstream.flush();
            outstream.close();
            instream.close();

            File file = new File(filepath);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void copyFilesParams() {
        try {

            String filepath = datapath + "/tessdata/ara.cube.params";
            AssetManager assetManager = getAssets();

            InputStream instream = assetManager.open("tessdata/ara.cube.params");
            OutputStream outstream = new FileOutputStream(filepath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }


            outstream.flush();
            outstream.close();
            instream.close();

            File file = new File(filepath);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void copyFilesSize() {
        try {

            String filepath = datapath + "/tessdata/ara.cube.size";
            AssetManager assetManager = getAssets();

            InputStream instream = assetManager.open("tessdata/ara.cube.size");
            OutputStream outstream = new FileOutputStream(filepath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }


            outstream.flush();
            outstream.close();
            instream.close();

            File file = new File(filepath);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void copyFilesWordFreq() {
        try {

            String filepath = datapath + "/tessdata/ara.cube.word-freq";
            AssetManager assetManager = getAssets();

            InputStream instream = assetManager.open("tessdata/ara.cube.word-freq");
            OutputStream outstream = new FileOutputStream(filepath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }


            outstream.flush();
            outstream.close();
            instream.close();

            File file = new File(filepath);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void copyFilesTrained() {
        try {

            String filepath = datapath + "/tessdata/ara.traineddata";
            AssetManager assetManager = getAssets();

            InputStream instream = assetManager.open("tessdata/ara.traineddata");
            OutputStream outstream = new FileOutputStream(filepath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }


            outstream.flush();
            outstream.close();
            instream.close();

            File file = new File(filepath);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Runnable runnable = new Runnable() {

        public void run() {

            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            UpdateTime = TimeBuff + MillisecondTime;

            Seconds = (int) (UpdateTime / 1000);

            Minutes = Seconds / 60;

            Seconds = Seconds % 60;

            MilliSeconds = (int) (UpdateTime % 1000);

            tvWaktuProses.setText("" + Minutes + ":"
                    + String.format("%02d", Seconds) + ":"
                    + String.format("%03d", MilliSeconds));

            handler.postDelayed(this, 0);
        }

    };

    private void startTimer(){
        StartTime = SystemClock.uptimeMillis();
        handler.postDelayed(runnable, 0);
    }

    private void resetTimer(){
        MillisecondTime = 0L ;
        StartTime = 0L ;
        TimeBuff = 0L ;
        UpdateTime = 0L ;
        Seconds = 0 ;
        Minutes = 0 ;
        MilliSeconds = 0 ;
        tvWaktuProses.setText("0");
    }

    private void pauseTimer(){
        handler.removeCallbacks(runnable);
    }

    private void showProgress(ProgressBar progressBar, LinearLayout linearLayout){
        progressBar.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.GONE);
    }

    private void stopProgres(ProgressBar progressBar,LinearLayout linearLayout){
        progressBar.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);
    }


}
