package com.skripsi.fahri.arab_text_recognition;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.skripsi.fahri.arab_text_recognition.utils.Constant;

public class HomeActivity extends AppCompatActivity {

    LinearLayout btnLansung,btnTidakLansung;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnLansung=findViewById(R.id.btnLansung);
        btnLansung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("status", Constant.lansung);
                startActivity(intent);
            }
        });

        btnTidakLansung=findViewById(R.id.btnTidakLansung);

        btnTidakLansung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getApplicationContext(),MainActivity.class);
                intent.putExtra("status", Constant.tidakLansung);
                startActivity(intent);
            }
        });
    }
}
