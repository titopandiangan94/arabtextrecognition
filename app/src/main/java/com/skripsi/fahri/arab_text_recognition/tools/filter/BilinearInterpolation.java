package com.skripsi.fahri.arab_text_recognition.tools.filter;

import android.graphics.Bitmap;
import android.graphics.Color;

public class BilinearInterpolation {
    public static Bitmap Resize(Bitmap bitmap){
        Bitmap bmp = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        int scale = 2;
        int imgWidth = bmp.getWidth();
        int imgHeight = bmp.getHeight();
        int scaleImgWidth = bmp.getWidth()*scale;
        int scaleImgHeight = bmp.getHeight()*scale;

        int[] intArray = new int[imgWidth*imgHeight];


        bmp.getPixels(intArray, 0, bmp.getWidth(), 0, 0, imgWidth, imgHeight);

        int[] resize = new int[scaleImgWidth*scaleImgHeight];
        double x_ratio = imgWidth/(double)scaleImgWidth ;
        double y_ratio = imgHeight/(double)scaleImgHeight ;
        double px, py ;
        for (int i=0;i<scaleImgHeight;i++) {
            for (int j=0;j<scaleImgWidth;j++) {
                px = Math.floor(j*x_ratio) ;
                py = Math.floor(i*y_ratio) ;
                resize[(i*scaleImgWidth)+j] = intArray[(int)((py*imgWidth)+px)] ;
            }
        }

        return Bitmap.createBitmap(resize, scaleImgWidth, scaleImgHeight, Bitmap.Config.ARGB_8888);
    }
}
