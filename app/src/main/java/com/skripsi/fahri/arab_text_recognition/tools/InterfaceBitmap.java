package com.skripsi.fahri.arab_text_recognition.tools;

import com.skripsi.fahri.arab_text_recognition.tools.filter.FastBitmap;

public interface InterfaceBitmap {
    void applyInPlace(FastBitmap fastBitmap);
}
